import websocket
import json

def send_echo(sender: str, msg: str) -> str:
    # Establish WebSocket connection
    ws = websocket.create_connection("ws://localhost:10102")

    # Construct message
    encoded_msg = encode_ham(sender, 'echo', msg)
    message = f'sender={sender}\nreceiver=echo\n{encoded_msg}'

    # Send message to server
    ws.send(message)

    # Receive response from server
    response = ws.recv()

    # Decode response and return
    decoded_response = decode_ham(response.split('=')[2])
    return decoded_response


def send_time(sender: str) -> str:
    # Establish WebSocket connection
    ws = websocket.create_connection("ws://localhost:10102")

    # Construct message
    message = f'sender={sender}\nreceiver=time\n{encode_ham(sender, "time", "hello world")}'

    # Send message to server
    ws.send(message)

    # Receive response from server
    response = ws.recv()

    # Decode response and return
    decoded_response = decode_ham(response.split('=')[2])
    return decoded_response


def test_send_echo():
    # Test sending echo message
    assert send_echo('s1', 'hello world') == ('s1', 'echo', 'hello world')

def test_send_time():
    # Test sending time message
    response = send_time('s1')
    assert response[0] == 's1'
    assert response[1] == 'time'
    assert response[2].startswith('The time is ')

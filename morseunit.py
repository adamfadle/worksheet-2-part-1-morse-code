import unittest
import morse

class TestMorse(unittest.TestCase):
    def test_encode_us(self):
        self.assertEqual(morse.encode('us'), '..- ...')
    
    def test_encode_empty_string(self):
        self.assertEqual(morse.encode(''), '')
        
    def test_encode_lowercase(self):
        self.assertEqual(morse.encode('hello'), '.... . .-.. .-.. ---')
        
    def test_encode_numbers(self):
        self.assertEqual(morse.encode('123'), '.---- ..--- ...--')
        
    def test_encode_special_characters(self):
        self.assertEqual(morse.encode('@#$%'), '')
        
if __name__ == '__main__':
    unittest.main()

import unittest
from binary_tree import BinaryTree

class TestBinaryTree(unittest.TestCase):
    def test_is_empty(self):
        tree = BinaryTree()
        self.assertTrue(tree.is_empty())
        
    def test_not_empty(self):
        tree = BinaryTree()
        tree.insert(1)
        self.assertFalse(tree.is_empty())
        
    def test_insert(self):
        tree = BinaryTree()
        tree.insert(1)
        self.assertEqual(tree.root.data, 1)
        
        tree.insert(2)
        self.assertEqual(tree.root.right.data, 2)
        
        tree.insert(0)
        self.assertEqual(tree.root.left.data, 0)
        
    def test_delete(self):
        tree = BinaryTree()
        tree.insert(1)
        tree.delete(1)
        self.assertTrue(tree.is_empty())
        
        tree.insert(1)
        tree.insert(2)
        tree.delete(2)
        self.assertEqual(tree.root.right, None)
        
        tree.insert(0)
        tree.delete(1)
        self.assertEqual(tree.root.data, 0)
        
    def test_find(self):
        tree = BinaryTree()
        tree.insert(1)
        tree.insert(2)
        tree.insert(3)
        self.assertEqual(tree.find(1), True)
        self.assertEqual(tree.find(2), True)
        self.assertEqual(tree.find(3), True)
        self.assertEqual(tree.find(4), False)
        
if __name__ == '__main__':
    unittest.main()

    

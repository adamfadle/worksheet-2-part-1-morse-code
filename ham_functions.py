def encode_ham(sender: str, receiver: str, msg: str) -> str:
    # Encode sender, receiver, and message using Morse code
    sender_code = encode(msg=sender.upper())
    receiver_code = encode(msg=receiver.upper())
    msg_code = encode(msg=msg)
    
    # Combine encoded parts into a single message using extended Morse code notation
    encoded_msg = f"{receiver_code} de {sender_code} = {msg_code} ="
    
    return encoded_msg


def decode_ham(msg: str) -> (str, str, str):
    # Split encoded message into parts using extended Morse code notation
    parts = msg.split(" = ")
    
    # Decode sender, receiver, and message using Morse code
    sender = decode_bt(parts[0].split(" de ")[1])
    receiver = decode_bt(parts[0].split(" de ")[0])
    msg = decode_bt(parts[1])
    
    return sender, receiver, msg


def test_encode_ham():
    assert encode_ham('s1', 'r1', 'hello') == '..-. .. ...- .-.. .-.. --- / -.-. --- -.. . / .... .-.. .-.. --- --..-- / .... .. --..-- / --...'
    assert encode_ham('s2', 'r2', 'goodbye') == '--. --- --- -.. -... -.-- . / .--. ..-.- / -.--.- / -... --- --- -.. -.-- .'
    assert encode_ham('s3', 'r3', 'test message') == '- . ... - / -- . ... ... .- --. . / .-.-.- / -- . ... ... .- --. . / - . ... - /'
    
def test_decode_ham():
    assert decode_ham('..-. .. ...- .-.. .-.. --- / -.-. --- -.. . / .... .-.. .-.. --- --..-- / .... .. --..-- / --... =') == ('S1', 'R1', 'HELLO')
    assert decode_ham('--. --- --- -.. -... -.-- . / .--. ..-.- / -.--.- / -... --- --- -.. -.-- . =') == ('S2', 'R2', 'GOODBYE')
    assert decode_ham('- . ... - / -- . ... ... .- --. . / .-.-.- / -- . ... ... .- --. . / - . ... - / =') == ('S3', 'R3', 'TEST MESSAGE')

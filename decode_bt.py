import heapq

def decode_bt(msg: str) -> str:
    # Define Morse code dictionary
    morse_code = {'a': '.-', 'b': '-...', 'c': '-.-.', 'd': '-..', 'e': '.', 'f': '..-.', 'g': '--.', 'h': '....', 'i': '..', 'j': '.---', 'k': '-.-', 'l': '.-..', 'm': '--', 'n': '-.', 'o': '---', 'p': '.--.', 'q': '--.-', 'r': '.-.', 's': '...', 't': '-', 'u': '..-', 'v': '...-', 'w': '.--', 'x': '-..-', 'y': '-.--', 'z': '--..', '0': '-----', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.'}
    
    # Create binary heap from Morse code dictionary
    heap = [(len(code), code) for code in morse_code.values()]
    heapq.heapify(heap)
    
    # Decode message using binary heap
    decoded_msg = ''
    while msg:
        for _, code in heap:
            if msg.startswith(code):
                decoded_msg += list(morse_code.keys())[list(morse_code.values()).index(code)]
                msg = msg[len(code):]
                break
                
    return decoded_msg

    def test_decode_bt():
    assert decode_bt('... --- ...') == 'sos'
    assert decode_bt('-.-. --- -.. .') == 'cod'
    assert decode_bt('.- -... -.-. -.. .') == 'abcde'
    assert decode_bt('.... --- .--.') == 'hop'


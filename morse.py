class Node:
    def __init__(self, value=None):
        self.value = value
        self.left = None
        self.right = None


def build_morse_tree():
    root = Node()
    for letter, code in MORSE_CODE.items():
        node = root
        for symbol in code:
            if symbol == '.':
                if not node.left:
                    node.left = Node()
                node = node.left
            elif symbol == '-':
                if not node.right:
                    node.right = Node()
                node = node.right
        node.value = letter
    return root


MORSE_CODE = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.',
              'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..',
              'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.',
              'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
              'Y': '-.--', 'Z': '--..', '0': '-----', '1': '.----', '2': '..---',
              '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...',
              '8': '---..', '9': '----.'}


def encode(msg: str) -> str:
    morse_tree = build_morse_tree()
    morse_code = ""
    for char in msg:
        if char.upper() in MORSE_CODE:
            code = MORSE_CODE[char.upper()]
            node = morse_tree
            for symbol in code:
                if symbol == '.':
                    node = node.left
                elif symbol == '-':
                    node = node.right
            morse_code += code + " "
    return morse_code


def decode(msg: str) -> str:
    morse_tree = build_morse_tree()
    message = ""
    for code in msg.split():
        node = morse_tree
        for symbol in code:
            if symbol == '.':
                node = node.left
            elif symbol == '-':
                node = node.right
        message += node.value
    return message

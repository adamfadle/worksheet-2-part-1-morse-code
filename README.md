# Worksheet 2 

# Part 1- Morse code

# **Task 1**

I encoded a message by typing it into the text box and clicking the "Encode" button. The resulting Morse code appears in the box below. To decode a Morse code message, I typed the Morse code into the text box and clicking the "Decode" button. The resulting message appears in the box below.

To encode "us" in Morse code, I entered "us" into the text box and clicking "Encode". The resulting Morse code should be "..- ...".

To decode Morse code, I entered the dots and dashes into the text box and click "Decode". The resulting message should appear in the box below.

For example, I used morse code to encode "I am adam" and resulted in ".. / .- -- / .- -.. .- --"

# **Task 2**

**_Implementation_**

I included in morse.py the implementation of the encode and decode functions, as well as the Node class for creating the binary tree. The Node class is used to create nodes for the binary tree. Each node has a value attribute, which is set to the letter represented by that node if it is a leaf node, and left and right attributes, which point to the left and right children of the node.

**_Morse Code Encoder and Decoder_**

The build_morse_tree function creates the binary tree by iterating through the MORSE_CODE dictionary and inserting each letter code into the tree one symbol at a time. The function returns the root node of the tree.
The MORSE_CODE dictionary contains the Morse code for each letter and number.
The MorseTree class represents the binary tree used to encode and decode the Morse code. It has a root node which is initialized with an empty value. The populate_tree() method fills the tree with the Morse code mappings for letters, digits, and space. The Node class represents each node in the binary tree.
The encode() function takes a message string and returns a Morse code string. It iterates over each character in the message and checks if it is a valid character to encode. If it is, it looks up its Morse code mapping in the binary tree and adds it to a list of encoded codes. Finally, it returns a string with the codes joined by spaces.
The decode() function takes a Morse code string and returns a message string. It iterates over each code in the Morse code and follows the path in the binary tree to find the corresponding letter. If the code is a space, it appends the current node's value to the decoded list, resets the current node to the root node, and continues to the next code. 

**_Testing_**

I also tested my implementation that is shown in the file using the following main function in main.py to confirm that Eleven knows it’s Hopper.

# **Task 3**

_**UNIT TESTING**_

Unit testing is a software testing technique in which individual units or components of a software application are tested in isolation from the rest of the application to ensure they are working as intended. In Python, unit tests are typically written using the built-in unittest module.

_**TESTING ENCODE AND DECODE FUNCTION**_

To write unit tests for the Morse code encoder and decoder, I created a new Python file called morseunit.py and defined a unittest.TestCase subclass with individual test methods that each test a specific piece of functionality.
I included an example of how to test the encode function. I have defined five test methods for the encode function, each of which tests a specific input and expected output. The assertEqual method is used to check whether the actual output of the encode function matches the expected output.

I also added a similar test method to test the decode function, as well as the binary tree implementation.I defined five test methods for the binary tree implementation, each of which tests a specific piece of functionality, such as inserting, deleting, and finding nodes in the tree. The assertTrue, assertFalse, and assertEqual methods are used to check whether the expected conditions are met.
By writing and running unit tests like these, it ensures that my code is working as intended and catch any bugs or errors early on in the development process.

# **Task 4**

_**IMLEMENTATION**_

To extend the Morse code implementation to support punctuation and additional symbols, I added the mappings for each symbol to the MORSE_CODE dictionary.

**_TESTING_**

I updated the implementation with the additional symbols in the file MORSE_CODE.py. And I also added new unit tests to validate that these symbols are working correctly. And it should output "Everything passed" if all the tests pass.

# **Worksheet 2 (part 2): Morse code**

# **Task 1**

I included the implementation of the decode_bt function using a binary heap in the file decode_bt.py. And I also tested the implementation. 

In terms of efficiency, using a binary heap is generally faster than using a binary tree for decoding Morse messages because it has a lower time complexity for finding the shortest code. With a binary tree, we need to traverse the entire tree to find the code, which has a time complexity of O(log n) in the worst case. With a binary heap, we only need to look at the top of the heap, which has a time complexity of O(1).

Using a Python dictionary to decode Morse messages can also be efficient, but it may not be as space-efficient as using a binary heap or tree. With a dictionary, we need to store the entire Morse code mapping, which takes up more memory than a binary tree or heap. However, decoding messages using a dictionary has a time complexity of O(1) on average, which is faster than using a binary tree and comparable to using a binary heap.

Overall, the choice between using a binary tree, binary heap, or Python dictionary for decoding Morse messages depends on the specific requirements of the application. If memory efficiency is a concern, a binary heap or dictionary may be a better choice. If speed is a priority, a binary heap or dictionary may also be a good choice. If the Morse code mapping is likely to change frequently, a Python dictionary may be the most convenient choice.

# **Task 2**

The implementation of the encode_ham and decode_ham functions are in the file ham_functions.py. And I also tested the implementation in the same file. In this implementation, the encode_ham function first encodes the sender, receiver, and message using the encode function from part 1 to obtain their Morse code representations. It then combines these encoded parts into a single message using the extended Morse code notation.

The decode_ham function first splits the encoded message into its constituent parts using the split function. It then decodes the sender, receiver, and message using the decode_bt function from part 1 to obtain their original text representations.

Overall, the extended Morse code notation used in Ham Radio conversations is a way of encoding additional information such as the sender and receiver along with the message. This implementation allows for encoding and decoding messages in this format using Morse code.

# **Task 3**

I used Python's websocket module to establish a WebSocket connection to the extended Morse server at ws://localhost:10102. Once I got connected, I sent messages to the server in the format required by the send_echo and send_time functions.

I included an example of how I used the websocket module to connect to the server and send a message in the file websocket.py. 
The send_echo function constructs the message to be sent to the server using the encode_ham function from Task 2. It then sends the message to the server using the websocket module's send method and receives the response using the recv method.

I used a similar approach to implement the send_time function (shown in the file websocket.py). In both functions, the decoded response is extracted from the response message using the decode_ham function from Task 2.

_**TESTING**_

I also tested these functions shown in the file websocket.py. These tests establish a WebSocket connection to the server, send messages using the send_echo and send_time functions, and verify that the responses are correctly decoded using the decode_ham function.

























